from bs4 import BeautifulSoup
import sys
import pandas as pd 
# A python 2+3 compatible solution
if sys.version_info[0] == 3:
    from urllib.request import urlopen
else:
    from urllib import urlopen


base_url = "http://quotes.lifehack.org/quote/p/"

quote_id= []
quotes = []
author = []

def quotefetch(count):
	# print(base_url + str(count))						
	page = urlopen((base_url + str(count)))	
	# print(page)			
	soup = BeautifulSoup(page, "html.parser")
	quote_id_search = soup.find('li',{ 'data-quote-id':count})
	# print(quote_id)
	quote_text_header = quote_id_search.find('h3')
	quote_text = quote_text_header.text.strip()
	print("Quote : " + quote_text)


	quote_author_header = quote_id_search.find('h4')
	quote_author = quote_author_header.text.strip()
	print("Author : " + quote_author)
	# quote_text = quote_id.text.strip()
	# print(quote_text)	
	quote_id.append(count)
	quotes.append(quote_text)
	author.append(quote_author)								


if __name__ == '__main__':
	count = 148951
	# count = 1
	while count < 148955:
		try:
			print(count)
			print("intialize fetching..for id:" ,count)
			quotefetch(count)
			print("after fetching...fot id:" , count)

			count = count+1
		except Exception as e:
			pass

	columns = {'Quote_id' : quote_id , 'Quotes' : quotes , 'Author' : author }
	df = pd.DataFrame(columns)
	df.to_csv('test.csv')
	print(df)



# ==================================
# code to extract from single page
# ==================================
# page = urlopen(base_url)
# soup = BeautifulSoup(page, "html.parser")		
# # id_soup = BeautifulSoup()
# quote_id = soup.find('li',{ 'data-quote-id':'91739'})
# quote_text = quote_id.text.strip()
# print(quote_text)




